package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Claw;

public class OpenClaw extends CommandBase
{
    Claw claw;
    
    /* 
     * This command will open claw on the robot
     */

    // we take the instance of the Claw created in RobotContainer (which this function calls "clawies") and save it to a reference this class has called "claw"
    public OpenClaw(Claw clawies) 
    {
        claw = clawies;
    }
    
    //We run the claw's function called "openClaw"
    @Override
    public void execute()
    {
        claw.openClaw();
    }

    //We immediately tell the Command Scheduler (The thing running the 20ms Loop) that this command can be done as soon as it runs
    @Override
    public boolean isFinished()
    {
        return true;
    }

}
