package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.subsystems.DriveSubsystem;

public class Autonomous extends SequentialCommandGroup{
    public Autonomous(DriveSubsystem drive) {
        addCommands(
            new AutoDriveTime(Constants.AUTO_DRIVE_SPEED, drive)
        );
    }
}