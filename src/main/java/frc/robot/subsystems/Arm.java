package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.SoftLimitDirection;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Arm extends SubsystemBase 
{
    CANSparkMax armMotor = new CANSparkMax(Constants.ARM_MOTOR_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);

    /*
     * This is our arm subsystem.
     * The constructor will sent encoder based limits on how far the arm can move up and down.
     * These values can be tweaked or ignored completly 
     * Might also want to allow mapping of this enabling encoder limit to a button if it is two flakey
     */

    public Arm()
    {
        if(Constants.ENABLE_ENCODER_LIMITS == true)
        {
            armMotor.enableSoftLimit(SoftLimitDirection.kForward, true);
            armMotor.enableSoftLimit(SoftLimitDirection.kReverse, true);
            armMotor.setSoftLimit(SoftLimitDirection.kForward, Constants.ARM_SOFT_LIMIT_FWD);
            armMotor.setSoftLimit(SoftLimitDirection.kReverse, Constants.ARM_SOFT_LIMIT_BKWD);
        }
    }

    public void moveArm(double speed)
    {
        //arm doesnt care which direction you want to move that is what your commands are for :) 
        armMotor.set(speed);
    }
}