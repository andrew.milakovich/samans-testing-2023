/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveSubsystem extends SubsystemBase {

    /* Master Talons for arcade drive */
    CANSparkMax leftMotorLeader = new CANSparkMax(Constants.LEFT_DRIVE_MOTOR_LEADER_CAN_ID,CANSparkMaxLowLevel.MotorType. kBrushless);
    CANSparkMax rightMotorLeader = new CANSparkMax(Constants.RIGHT_DRIVE_MOTOR_LEADER_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);

    /* Follower Talons + Victors for six motor drives */
    CANSparkMax leftFollow1 = new CANSparkMax(Constants.LEFT_DRIVE_MOTOR_FOLLOW1_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);
    CANSparkMax leftFollow2 = new CANSparkMax(Constants.LEFT_DRIVE_MOTOR_FOLLOW2_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);
    CANSparkMax rightFollow1 = new CANSparkMax(Constants.RIGHT_DRIVE_MOTOR_FOLLOW1_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);
    CANSparkMax rightFollow2 = new CANSparkMax(Constants.RIGHT_DRIVE_MOTOR_FOLLOW2_CAN_ID,CANSparkMaxLowLevel.MotorType.kBrushless);
    double currentSpeed;

    DifferentialDrive robotDrive;

    public DriveSubsystem() 
    {
        currentSpeed = 0;
        
        //reset any configs on the motors so we can set it in code
        leftMotorLeader.restoreFactoryDefaults();
        rightMotorLeader.restoreFactoryDefaults();
        leftFollow1.restoreFactoryDefaults();
        leftFollow2.restoreFactoryDefaults();
        rightFollow1.restoreFactoryDefaults();
        rightFollow2.restoreFactoryDefaults();

        //set current limiting (aka do not let the motors take more power then we have available)
        leftMotorLeader.setSmartCurrentLimit(Constants.PEAK_MOTOR_AMPS);
        rightMotorLeader.setSmartCurrentLimit(Constants.PEAK_MOTOR_AMPS);
        
        /**
         * Drive robot forward and make sure all motors spin the correct way.
         * Toggle booleans accordingly.... 
         */

        leftMotorLeader.setInverted(true);
        rightMotorLeader.setInverted(true);

        /**
         * Have other motors follow a leader so we dont need to set everything on all six motors
         */
        leftFollow1.follow(leftMotorLeader);
        leftFollow2.follow(leftMotorLeader);

        rightFollow1.follow(rightMotorLeader);
        rightFollow2.follow(rightMotorLeader);

        robotDrive = new DifferentialDrive(leftMotorLeader, rightMotorLeader);
    }

    public void driveSubInit() {

       


    }

    //Send the raw number directly to the motors without any ramping
    public void directDrive(double y, double x) 
    {
        SmartDashboard.putNumber("DIRECT DRIVE X", x);
        SmartDashboard.putNumber("DIRECT DRIVE Y", y);
        robotDrive.arcadeDrive(y, x, false);

    }

    // This method controls the speed at which the motors accellerate.
    // This is helpful by preventing rocking back and forth as well as, 
    // potentially overheating the motors by going from full forward to full backwards too often
    // remember "forward" is from 0-1.0 and "backwards" is from -1.0-0

    public void rampDrive(double y, double x) 
    {
        double desiredSpeed = y; //desired speed "y" is based on y axis of joystick
        double accellerationNeeded = 0; // assume we dont need to accelerate/decelerate 
        
        if (desiredSpeed != currentSpeed) // our current and desired speed do not match so see how much we want too accelerate/decelerate by
        {
            accellerationNeeded = desiredSpeed - currentSpeed;

            if (accellerationNeeded >= Constants.DRIVE_MOTOR_MAX_FWD) //acceleration is greater than our allowed max Forward speed; set accelleration to our max allowed forward speed
            {
                accellerationNeeded = Constants.DRIVE_MOTOR_MAX_FWD;
            } 
            
            else if (accellerationNeeded <= Constants.DRIVE_MOTOR_MAX_BKWD) //acceleration is less than our allowed max backwards speed; set accelleration to our max allwed backwards speed 
            {
                accellerationNeeded = -Constants.DRIVE_MOTOR_MAX_BKWD;
            }    
        }
        currentSpeed = currentSpeed + accellerationNeeded;
        robotDrive.arcadeDrive(currentSpeed, x, false);

    }
}
